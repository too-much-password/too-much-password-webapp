const {expect} = require('chai');

module.exports = {
  'should display authentication page': function(client) {
    client
      .url(process.env.APPLICATION_URL)
      .useCss()
      .waitForElementPresent('body', 1000)
      .waitForElementPresent('body.pg-loaded', 10000)
      .waitForElementPresent('input#authentication_input_email', 1000)
      .waitForElementPresent('input#authentication_input_password', 1000)
      .waitForElementPresent('#authentication_btn_login[disabled]', 1000)
      .waitForElementPresent('#authentication_btn_register[disabled]', 1000)
      .setValue('input#authentication_input_email', 'user@example.com')
      .setValue('input#authentication_input_password', 'userpassword')
      .waitForElementPresent('#authentication_btn_login', 1000)
      .waitForElementPresent('#authentication_btn_register', 1000)
  },
  'should create the user and log in': function(client) {
    client
      .useCss()
      .click('#authentication_btn_register')
      .waitForElementPresent('#keypair_btn', 1000)
      .waitForElementPresent('#passwordsearch_input', 1000)
  },
  'should generate a public and a secret key': function(client) {
    client
      .useCss()
      .click('#keypair_btn')
      .useXpath()
      .waitForElementPresent('//*[@id="keypaircard_item_public"]//span[contains(text(),"clear")]', 1000)
      .waitForElementPresent('//*[@id="keypaircard_item_private"]//span[contains(text(),"clear")]', 1000)
      .useCss()
      .waitForElementPresent('#keypaircard_item_generate', 1000)
      .click('#keypaircard_item_generate')
      .useXpath()
      .waitForElementPresent('//*[@id="keypaircard_item_public"]//span[contains(text(),"check")]', 120000)
      .waitForElementPresent('//*[@id="keypaircard_item_private"]//span[contains(text(),"check")]', 120000)
  },
  'should go back to home page': function(client) {
    client
      .useXpath()
      .click('//*[@id="header-bar"]//span[contains(text(),"home")]')
      .useCss()
      .waitForElementPresent('#passwordcreatebutton', 1000)
      .waitForElementPresent('#passwordsearch_input', 1000)
  },
  'should show password creation dialog': function(client) {
    client
      .useCss()
      .click('#passwordcreatebutton')
      .waitForElementPresent('.password-edition-dialog_title', 1000)
      .waitForElementPresent('.password-edition-dialog_username', 1000)
      .waitForElementPresent('.password-edition-dialog_url', 1000)
      .waitForElementPresent('.password-edition-dialog_content', 1000)
  },
  'should create a password': function(client) {
    client
      .useCss()
      .setValue('.password-edition-dialog_title input', 'gitlab')
      .setValue('.password-edition-dialog_username input', 'username@example.com')
      .setValue('.password-edition-dialog_url input', 'https://gitlab.com')
      .setValue('.password-edition-dialog_content input', 'my_password')
      .click('.password-edition-dialog_submit')
      .waitForElementPresent('.password-list-item', 5000)
  },
  'should display the password in a dialog': function(client) {
    client
      .useCss()
      .click('.password-list-item')
      .waitForElementPresent('#password-display-dialog_content', 1000)
      .waitForElementPresent('#password-display-dialog_textfield', 60000)
      .expect.element('#password-display-dialog_textfield').to.have.value.that.equals('my_password')
  },
  'should close the dialog': function(client) {
    client
      .useCss()
      .click('.password-display-dialog_close')
      .waitForElementNotPresent('#password-display-dialog_content', 1000)
  },
  'close browser': function(client) {
    client.end();
  },
};
