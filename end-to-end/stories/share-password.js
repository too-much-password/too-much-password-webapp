const {expect} = require('chai');

module.exports = {
  'should display authentication page': function(client) {
    client
      .url(process.env.APPLICATION_URL)
      .useCss()
      .waitForElementPresent('body', 1000)
      .waitForElementPresent('body.pg-loaded', 10000)
      .waitForElementPresent('input#authentication_input_email', 1000)
      .waitForElementPresent('input#authentication_input_password', 1000)
      .waitForElementPresent('#authentication_btn_login[disabled]', 1000)
      .waitForElementPresent('#authentication_btn_register[disabled]', 1000)
      .setValue('input#authentication_input_email', 'friend@example.com')
      .setValue('input#authentication_input_password', 'friendpassword')
      .waitForElementPresent('#authentication_btn_login', 1000)
      .waitForElementPresent('#authentication_btn_register', 1000)
  },
  'should create the user and log in': function(client) {
    client
      .useCss()
      .click('#authentication_btn_register')
      .waitForElementPresent('#keypair_btn', 1000)
      .waitForElementPresent('#passwordsearch_input', 1000)
  },
  'should generate a public and a secret key': function(client) {
    client
      .useCss()
      .click('#keypair_btn')
      .useXpath()
      .waitForElementPresent('//*[@id="keypaircard_item_public"]//span[contains(text(),"clear")]', 1000)
      .waitForElementPresent('//*[@id="keypaircard_item_private"]//span[contains(text(),"clear")]', 1000)
      .useCss()
      .waitForElementPresent('#keypaircard_item_generate', 1000)
      .click('#keypaircard_item_generate')
      .useXpath()
      .waitForElementPresent('//*[@id="keypaircard_item_public"]//span[contains(text(),"check")]', 120000)
      .waitForElementPresent('//*[@id="keypaircard_item_private"]//span[contains(text(),"check")]', 120000)
  },
  'should go back to home page': function(client) {
    client
      .useXpath()
      .click('//*[@id="header-bar"]//span[contains(text(),"home")]')
      .useCss()
      .waitForElementPresent('#passwordcreatebutton', 1000)
      .waitForElementPresent('#passwordsearch_input', 1000)
  },
  'should show password creation dialog': function(client) {
    client
      .useCss()
      .click('#passwordcreatebutton')
      .waitForElementPresent('.password-edition-dialog_title', 1000)
      .waitForElementPresent('.password-edition-dialog_username', 1000)
      .waitForElementPresent('.password-edition-dialog_url', 1000)
      .waitForElementPresent('.password-edition-dialog_content', 1000)
  },
  'should create a password': function(client) {
    client
      .useCss()
      .setValue('.password-edition-dialog_title input', 'gitlab')
      .setValue('.password-edition-dialog_username input', 'friend@example.com')
      .setValue('.password-edition-dialog_url input', 'https://gitlab.com')
      .setValue('.password-edition-dialog_content input', 'my_password')
      .click('.password-edition-dialog_submit')
      .waitForElementPresent('.password-list-item', 5000)
  },
  'should show share button': function(client) {
    client
      .useCss()
      .click('.password-list-item:first-of-type .password-list-item_menu-btn')
      .waitForElementPresent('.password-list-item_btn-delete', 1000)
      .waitForElementPresent('.password-list-item_btn-share', 1000)
  },
  'should redirect to the sharing page': function(client) {
    client
      .useCss()
      .click('.password-list-item_btn-share')
      .waitForElementPresent('#password-share-view', 1000)
      .waitForElementPresent('#password-share-view .password-shared-list', 1000)
      .waitForElementPresent('#password-share-view .share-field_input', 1000)
      .waitForElementPresent('#password-share-view .share-field_button', 1000)
      .waitForElementPresent('#password-share-view .share-field_button .share-field_search-icon', 1000)
  },
  'should search for another people': function(client) {
    client
      .useCss()
      .setValue('#password-share-view .share-field_input input', 'user@example.com')
      .useXpath()
      .waitForElementPresent('//*[@id="password-share-view"]//span[contains(text(),"add")]', 5000)
  },
  'should create the new cipher key': function(client) {
    client
      .useCss()
      .click('#password-share-view .share-field_button')
      .waitForElementPresent('#password-share-view .share-field_search-icon', 5000)
  },
  'close browser': function(client) {
    client.end();
  },
};
