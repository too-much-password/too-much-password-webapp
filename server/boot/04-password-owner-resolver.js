'use strict';

const _ = require('lodash');
const debug = require('debug')('too-much-password:resolver:password-owner');

module.exports = function(server) {
  let Role = server.models.Role;
  Role.registerResolver('password-owner', function(role, ctx, next) {
    if (ctx.modelName !== 'Password') {
      debug(`${ctx.modelName} is not a Password`);
      return next(null, false);
    }
    if (!(ctx.accessToken && ctx.accessToken.userId)) {
      debug('not authenticated', ctx.accessToken);
      return next(null, false);
    }
    ctx.model
      .count({
        id: ctx.modelId,
        ownerId: ctx.accessToken.userId
      })
      .then(res => {
        debug(`${res} ${ctx.modelName} found`);
        return next(null, res > 0)
      });
  });
}
