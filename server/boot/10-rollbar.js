'use strict';

const rollbar = require('rollbar');

module.exports = function(server) {
  if (process.env.ROLLBAR_ENABLED && process.env.ROLLBAR_SERVER_ACCESSTOKEN) {
    rollbar.init(process.env.ROLLBAR_SERVER_ACCESSTOKEN, {
      environment: process.env.NODE_ENV,
    });
    server.get('remoting').errorHandler = {
      handler: function(err, req, res, defaultHandler) {
        rollbar.handleError(err, req);
        defaultHandler(err);
      },
    };
  }
};
