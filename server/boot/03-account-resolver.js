'use strict';

const _ = require('lodash');
const debug = require('debug')('too-much-password:resolver:account');

module.exports = function(server) {
  let Role = server.models.Role;
  Role.registerResolver('account', function(role, ctx, next) {
    if (ctx.modelName !== 'Account') {
      debug('not an Account');
      return next(null, false);
    }
    if (!(ctx.accessToken && ctx.accessToken.userId)) {
      debug('not authenticated', ctx.accessToken);
      return next(null, false);
    }
    if (ctx.accessToken.userId !== ctx.modelId) {
      debug('not the same account');
      return next(null, false);
    }
    return next(null, true);
  });
}
