'use strict';

const accountUtils = require('./account.test-utils');

const app = require('../server');
const expect = require('chai').expect;
const request = require('supertest');

describe('[MODEL] Accounts', function() {

  afterEach(() => app.models.Account.destroyAll());
  afterEach(() => app.models.AccessToken.destroyAll());

  describe('register function', function() {

    it('should register when email is valid', (done) => {
      accountUtils.create(app, {
        email: 'user@example.com',
        password: 'an_awesome_password'
      }).then((res) => {
        accountUtils.expectToBeAnAccount(res.body);
        done();
      }).catch(done);
    });

    it('should not register when email is invalid', (done) => {
      accountUtils.create(app, {
        email: 'user',
        password: 'an_awesome_password'
      }).catch((err) => {
        expect(err).to.exist;
        done();
      })
    });

    it('should not register if email already exist', (done) => {
      accountUtils.create(app, {
        email: 'user@example.com',
        password: 'an_awesome_password'
      }).then((res) => {
        accountUtils.expectToBeAnAccount(result.body);
        return accountUtils.create(app, {
          email: 'user@example.com',
          password: 'an_awesome_password'
        });
      }).catch((err) => {
        expect(err).to.exist;
        done();
      })
    });

  });

  describe('login function', function() {

    it('should login if registered', (done) => {
      accountUtils.create(app, {
        email: 'user@example.com',
        password: 'an_awesome_password'
      }).then((res) => {
        accountUtils.expectToBeAnAccount(res.body);
        return accountUtils.login(app, {
          email: 'user@example.com',
          password: 'an_awesome_password'
        })
      }).then((res) => {
        accountUtils.expectToBeAnAccessToken(res.body);
        done();
      });
    });

    it('should not login if not registered', (done) => {
      accountUtils.login(app, {
        email: 'user@example.com',
        password: 'an_awesome_password'
      }).catch((err) => {
        expect(err).to.exist;
        done();
      });
    });

  });

});
