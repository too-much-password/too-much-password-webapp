'use strict';

const accountUtils = require('./account.test-utils');
const cipherKeyUtils = require('./cipher-key.test-utils');
const passwordUtils = require('./password.test-utils');

const app = require('../server');
const expect = require('chai').expect;
const request = require('supertest');

describe('[MODEL] CipherKeys', function() {

  this.accounts = {
    real: {
      account: null,
      token: null
    },
    friend: {
      account: null,
      token: null
    },
    other: {
      account: null,
      token: null
    }
  };

  afterEach(() => app.models.Account.destroyAll());
  afterEach(() => app.models.AccessToken.destroyAll());
  afterEach(() => app.models.CipherKey.destroyAll());
  afterEach(() => app.models.Password.destroyAll());

  beforeEach((done) => {
    app.models.Account.create({
      email: 'real@example.com',
      password: 'password'
    }).then((account) => {
      accountUtils.expectToBeAnAccount(account);
      this.accounts.real.account = account;
      return app.models.Account.login({
        email: 'real@example.com',
        password: 'password'
      })
    }).then((token) => {
      accountUtils.expectToBeAnAccessToken(token);
      this.accounts.real.token = token;
      done();
    }).catch(done)
  });

  beforeEach((done) => {
    app.models.Account.create({
      email: 'friend@example.com',
      password: 'password'
    }).then((account) => {
      accountUtils.expectToBeAnAccount(account);
      this.accounts.other.account = account;
      return app.models.Account.login({
        email: 'friend@example.com',
        password: 'password'
      })
    }).then((token) => {
      accountUtils.expectToBeAnAccessToken(token);
      this.accounts.friend.token = token;
      done();
    });
  });

  beforeEach((done) => {
    app.models.Account.create({
      email: 'other@example.com',
      password: 'password'
    }).then((account) => {
      accountUtils.expectToBeAnAccount(account);
      this.accounts.other.account = account;
      return app.models.Account.login({
        email: 'other@example.com',
        password: 'password'
      })
    }).then((token) => {
      accountUtils.expectToBeAnAccessToken(token);
      this.accounts.other.token = token;
      done();
    });
  });

  describe('create function', () => {

    describe('owner', () => {

      it('should create a cipher key', (done) => {
        passwordUtils.create(app, this.accounts.real.token, {
          title: 'test',
          content: 'blablabla'
        }).then((res) => {
          return cipherKeyUtils.create(app, this.accounts.real.token, res.body.id, {
            accountId: this.accounts.friend.token.userId,
            content: 'yolooooo'
          })
        }).then((res) => {
          expect(res.status).to.eql(200);
          cipherKeyUtils.expectToBeACipherKey(res.body);
          done();
        }).catch(done)
      });

    });

    describe('other', () => {

      it('should not create a cipher key', (done) => {
        passwordUtils.create(app, this.accounts.real.token, {
          title: 'test',
          content: 'blablabla'
        }).then((res) => {
          return cipherKeyUtils.create(app, this.accounts.other.token, res.body.id, {
            accountId: this.accounts.friend.token.userId,
            content: 'yolooooo'
          })
        }).then((res) => {
          expect(res.status).to.eql(401);
          done();
        }).catch(done)
      });

    });

    describe('find function', () => {

      beforeEach((done) => {
        passwordUtils.create(app, this.accounts.real.token, {
          title: 'test',
          content: 'blablabla'
        }).then((res) => {
          return cipherKeyUtils.create(app, this.accounts.real.token, res.body.id, {
            accountId: this.accounts.real.token.userId,
            content: 'yolooooo'
          })
        }).then((res) => {
          expect(res.status).to.eql(200);
          cipherKeyUtils.expectToBeACipherKey(res.body);
          done();
        }).catch(done)
      });

      describe('owner', () => {

        it('should be allowed to get his passwords through the cipher keys', (done) => {
          passwordUtils
            .fetch(app, this.accounts.real.token)
            .then(res => {
              expect(res.status).to.eql(200);
              res.body.forEach(item => passwordUtils.expectToBeAPassword(item));
              done();
            }).catch(done);
        });

      });

      describe('other', () => {

        it('should not be allowed to get other passwords through the cipher keys', (done) => {
          passwordUtils
            .fetch(app, {
              id: this.accounts.other.token.id,
              userId: this.accounts.real.token.userId,
            })
            .then(res => {
              expect(res.status).to.eql(401);
              done();
            }).catch(done);
        });

      });

    });

  });

});
