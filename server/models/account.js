module.exports = function(Account) {

  Account.disableRemoteMethodByName('createChangeStream', true);

  Account.search = function(email) {
    return Account.findOne({
      fields: ['id', 'email', 'publicKey'],
      where: {email}
    });
  };

  Account.remoteMethod('search', {
    accepts: [
      {
        arg: 'email',
        description: 'Email of the account',
        required: true,
        type: 'string',
      }
    ],
    accessType: 'READ',
    description: 'Search an account by mail',
    returns: {arg: 'account', type: 'Account', root: true},
    http: {path: '/search/:email', verb: 'get'}
  });

};
