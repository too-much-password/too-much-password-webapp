const expect = require('chai').expect;
const request = require('supertest');

function expectToBeAnAccount(result) {
  expect(result).to.be.an('object');
  expect(result).to.have.property('id');
  expect(result).to.have.property('email');
  expect(result.id).to.be.a('number');
  expect(result.email).to.be.a('string');
}

function expectToBeAnAccessToken(result) {
  expect(result).to.be.an('object');
  expect(result).to.have.property('id');
  expect(result).to.have.property('ttl');
  expect(result).to.have.property('userId');
  expect(result.id).to.be.a('string');
  expect(result.userId).to.be.a('number');
  expect(result.ttl).to.be.a('number');
}

function create(app, account) {
  return new Promise((resolve, reject) => {
    request(app)
      .post('/api/accounts')
      .send(account)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(res);
        }
      })
  })
}

function login(app, credentials) {
  return new Promise((resolve, reject) => {
    request(app)
      .post('/api/accounts/login')
      .send(credentials)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(res);
        }
      })
  })
}

module.exports = {
  create,
  expectToBeAnAccount,
  expectToBeAnAccessToken,
  login
};
