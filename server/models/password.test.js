'use strict';

const accountUtils = require('./account.test-utils');
const passwordUtils = require('./password.test-utils');
const expect = require('chai').expect;
const app = require('../server');

describe('[MODEL] Passwords', function() {

  let accounts = {
    real: {
      account: null,
      token: null
    },
    friend: {
      account: null,
      token: null
    },
    other: {
      account: null,
      token: null
    }
  };

  afterEach(() => app.models.Account.destroyAll());
  afterEach(() => app.models.AccessToken.destroyAll());
  afterEach(() => app.models.CipherKey.destroyAll());
  afterEach(() => app.models.Password.destroyAll());

  beforeEach((done) => {
    app.models.Account.create({
      email: 'real@example.com',
      password: 'password'
    }).then((account) => {
      accountUtils.expectToBeAnAccount(account);
      accounts.real.account = account;
      return app.models.Account.login({
        email: 'real@example.com',
        password: 'password'
      })
    }).then((token) => {
      accountUtils.expectToBeAnAccessToken(token);
      accounts.real.token = token;
      done();
    });
  });

  beforeEach((done) => {
    app.models.Account.create({
      email: 'friend@example.com',
      password: 'password'
    }).then((account) => {
      accountUtils.expectToBeAnAccount(account);
      accounts.other.account = account;
      return app.models.Account.login({
        email: 'friend@example.com',
        password: 'password'
      })
    }).then((token) => {
      accountUtils.expectToBeAnAccessToken(token);
      accounts.friend.token = token;
      done();
    });
  });

  beforeEach((done) => {
    app.models.Account.create({
      email: 'other@example.com',
      password: 'password'
    }).then((account) => {
      accountUtils.expectToBeAnAccount(account);
      accounts.other.account = account;
      return app.models.Account.login({
        email: 'other@example.com',
        password: 'password'
      })
    }).then((token) => {
      accountUtils.expectToBeAnAccessToken(token);
      accounts.other.token = token;
      done();
    });
  });

  describe('create function', function() {

    it('should create the password if authenticated', (done) => {
      passwordUtils.create(app, accounts.real.token, {
        title: 'test',
        content: 'blablabla'
      }).then((res) => {
        expect(res.status).to.eql(200);
        passwordUtils.expectToBeAPassword(res.body);
        done();
      }).catch(done)
    });

    it('should not create password for an other user', (done) => {
      passwordUtils.create(app, accounts.real.token, {
        title: 'test',
        content: 'blablabla',
        ownerId: accounts.other.token.userId
      }).then(res => {
        expect(res.status).to.eql(200);
        passwordUtils.expectToBeAPassword(res.body);
        expect(res.body.ownerId).to.eql(accounts.real.token.userId);
        done();
      })
    });

  });

  describe('edit function', function() {

    beforeEach((done) => {
      passwordUtils.create(app, accounts.real.token, {
        title: 'test',
        content: 'blablabla'
      }).then((res) => {
        expect(res.status).to.eql(200);
        passwordUtils.expectToBeAPassword(res.body);
        this.password = res.body;
        done();
      }).catch(done)
    });

    describe('as owner', () => {

      it('should edit the password title', (done) => {
        passwordUtils.update(app, accounts.real.token, this.password.id, {
          title: 'test2'
        }).then((res) => {
          expect(res.status).to.eql(200);
          passwordUtils.expectToBeAPassword(res.body);
          expect(res.body.title).to.eql('test2');
          done();
        }).catch(done)
      });

      it('should edit the password content', (done) => {
        passwordUtils.update(app, accounts.real.token, this.password.id, {
          content: 'bliblibli'
        }).then((res) => {
          expect(res.status).to.eql(200);
          passwordUtils.expectToBeAPassword(res.body);
          expect(res.body.content).to.eql('bliblibli');
          done();
        }).catch(done)
      });

    });

    describe('as other', () => {

      it('should not edit the password', (done) => {
        let token = {
          id: accounts.other.token.id,
          userId: accounts.real.token.userId
        };
        passwordUtils.update(app, token, this.password.id, {
          title: 'test2'
        }).then(res => {
          expect(res.status).to.eql(401);
          done();
        }).catch(done)
      });

    });

  });

});
