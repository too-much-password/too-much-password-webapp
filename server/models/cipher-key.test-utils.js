const expect = require('chai').expect;
const request = require('supertest');

function expectToBeACipherKey(result) {
  expect(result).to.be.an('object');
  expect(result).to.have.property('id');
}

function create(app, token, passwordId, key) {
  return new Promise((resolve, reject) => {
    request(app)
      .post(`/api/passwords/${passwordId}/cipherKeys`)
      .set('Authorization', token.id)
      .send(key)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        if (err) return reject(err);
        resolve(res);
      });
  });
}

module.exports = {
  create,
  expectToBeACipherKey,
};
