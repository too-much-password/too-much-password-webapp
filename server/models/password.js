module.exports = function(Password) {
  Password.disableRemoteMethodByName('create', true);
  Password.disableRemoteMethodByName('upsert', true);
  Password.disableRemoteMethodByName('updateAll', true);
  Password.disableRemoteMethodByName('updateAttributes', false);
  Password.disableRemoteMethodByName('createChangeStream', true);
};
