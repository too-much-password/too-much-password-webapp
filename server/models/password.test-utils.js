'use strict';

const expect = require('chai').expect;
const Promise = require('bluebird');
const request = require('supertest');

function expectToBeAPassword(result) {
  expect(result).to.be.an('object');
  expect(result).to.have.property('id');
  expect(result).to.have.property('ownerId');
  expect(result).to.have.property('title');
  expect(result).to.have.property('content');
  expect(result.id).to.be.a('number');
  expect(result.ownerId).to.be.a('number');
  expect(result.title).to.be.a('string');
  expect(result.content).to.be.a('string');
}

function create(app, token, password) {
  return new Promise((resolve, reject) => {
    request(app)
      .post(`/api/accounts/${token.userId}/ownedPasswords`)
      .set('Authorization', token.id)
      .send({
        ownerId: token.userId,
        ...password
      })
      .expect('Content-Type', /json/)
      .end((err, res) => {
        if (err) return reject(err);
        resolve(res);
      });
  });
}

function fetch(app, token) {
  return new Promise((resolve, reject) => {
    let filter = {include: ['password']};
    request(app)
      .get(`/api/accounts/${token.userId}/passwords`)
      .set('Authorization', token.id)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        if (err) return reject(err);
        resolve(res);
      });
  });
}

function fetchThroughCipherKey(app, token) {
  return new Promise((resolve, reject) => {
    let filter = {
      include: ['password']};
    request(app)
      .get(`/api/accounts/${token.userId}/cipherKeys?filter=%7B%22include%22%3A%5B%22password%22%5D%7D`)
      .set('Authorization', token.id)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        if (err) return reject(err);
        resolve(res);
      });
  });
}

function update(app, token, passwordId, data) {
  return new Promise((resolve, reject) => {
    request(app)
      .put(`/api/accounts/${token.userId}/ownedPasswords/${passwordId}`)
      .set('Authorization', token.id)
      .send(data)
      .expect('Content-Type', /json/)
      .end((err, res) => {
        if (err) return reject(err);
        resolve(res);
      });
  });
};

module.exports = {
  create,
  update,
  fetch,
  fetchThroughCipherKey,
  expectToBeAPassword
};
