CREATE TABLE acl (
  id SERIAL NOT NULL PRIMARY KEY,
  model TEXT,
  property TEXT,
  accesstype TEXT,
  permission TEXT,
  principaltype TEXT,
  principalid TEXT
);

CREATE TABLE role (
  id SERIAL NOT NULL PRIMARY KEY,
  name TEXT NOT NULL,
  description TEXT,
  created TIMESTAMP NOT NULL DEFAULT NOW(),
  modified TIMESTAMP
);

CREATE TABLE rolemapping (
  id SERIAL NOT NULL PRIMARY KEY,
  principaltype TEXT NOT NULL,
  principalid TEXT NOT NULL,
  roleid INTEGER NOT NULL REFERENCES role(id)
);

CREATE TABLE account (
  id SERIAL NOT NULL PRIMARY KEY,
  email TEXT NOT NULL UNIQUE,
  password TEXT NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT NOW(),
  lastUpdated TIMESTAMP
);

CREATE TABLE accesstoken (
  id TEXT NOT NULL PRIMARY KEY,
  userid INTEGER NOT NULL
  REFERENCES account(id)
  ON DELETE CASCADE,
  ttl INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT NOW()
);
