import { applyMiddleware, createStore, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

import persistState, { mergePersistedState } from 'redux-localstorage';
import adapter from 'redux-localstorage/lib/adapters/localStorage';
import filter from 'redux-localstorage-filter';

import rootReducer from './reducers';

export default function (history) {
  const logger = createLogger({
    level: process.env.NODE_ENV === 'production' ? 'error' : 'log',
    collapsed: true,
  });
  const reduxRouterMiddleware = routerMiddleware(history);

  const reducers = compose(mergePersistedState())(rootReducer);

  const storageAccount = compose(filter('account'))(adapter(window.localStorage));
  const storageAuthentication = compose(filter('authentication'))(adapter(window.localStorage));
  const storageLanguage = compose(filter('language'))(adapter(window.localStorage));

  let createCustomStore = compose(
    persistState(storageAccount, 'account'),
    persistState(storageAuthentication, 'authentication'),
    persistState(storageLanguage, 'language'),
  )(createStore);

  createCustomStore = applyMiddleware(
    reduxRouterMiddleware,
    thunk,
    logger,
  )(createCustomStore);

  const store = createCustomStore(reducers);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducers', () => {
      const nextReducer = require('./reducers'); // eslint-disable-line global-require
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}
