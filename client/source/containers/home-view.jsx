import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import lodash from 'lodash';

import AccountEffect from '../effects/account';

import KeypairButton from '../components/account-keypair-button';
import PasswordSearchField from '../components/password-search-field';
import PasswordList from '../components/password-list';
import PasswordCreateButton from '../components/password-create-button';

import logger from '../services/logger';

class HomeView extends Component {

  static propTypes = {
    account: PropTypes.shape({
      id: PropTypes.number,
      publicKey: PropTypes.string,
    }),
    accountEffect: PropTypes.shape({
      cipherKeys$delete: PropTypes.func.isRequired,
      cipherKeys$find: PropTypes.func.isRequired,
      ownedPasswords$delete: PropTypes.func.isRequired,
      passwords$find: PropTypes.func.isRequired,
    }).isRequired,
    publicKey: PropTypes.shape({
      content: PropTypes.string,
    }),
    authenticationActions: PropTypes.shape({
      logout: PropTypes.func.isRequired,
    }),
    authentication: PropTypes.shape({
      id: PropTypes.string.isRequired,
      userId: PropTypes.number.isRequired,
    }),
    passwords: PropTypes.arrayOf(PropTypes.shape({
      content: PropTypes.string.isRequired,
    })).isRequired,
    privateKey: PropTypes.shape({
      content: PropTypes.string,
    }).isRequired,
    routerActions: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
  };

  constructor(props) {
    super(props);
    this.state = {
      query: '',
    };
  }

  componentDidMount() {
    Promise.all([
      this.props.accountEffect
        .cipherKeys$find(this.props.authentication.userId),
      this.props.accountEffect
        .passwords$find(this.props.authentication.userId),
    ])
      .catch((err) => {
        if (err.data && err.data.error && err.data.error.code
          && err.data.error.code === 'AUTHORIZATION_REQUIRED') {
          this.props.authenticationActions.logout();
        } else {
          logger.error(err);
          throw err;
        }
      });
  }

  onTouchCreate() {
    return this.props.routerActions.push('/password/create');
  }

  onDeletePassword(password) {
    if (this.props.authentication.userId === password.ownerId) {
      return this.props.accountEffect.ownedPasswords$delete(
        this.props.authentication.userId,
        password.id,
      );
    }
    return this.props.accountEffect.cipherKeys$delete(
      this.props.authentication.userId,
      password.id,
    );
  }

  onSharePassword(password) {
    this.props.routerActions.push(`/password/${password.id}/share`);
  }

  onSelectPassword(password) {
    return this.props.routerActions.push(`/password/${password.id}/show`);
  }

  onQueryChange(query) {
    this.setState({ query });
  }

  creationComponent() {
    const rowPaddingStyle = { paddingTop: 10, paddingMargin: 10 };
    if (this.props.privateKey && this.props.account && this.props.account.publicKey) {
      return (
        <div className="row center-xs" style={rowPaddingStyle}>
          <div className="col-xs">
            <PasswordCreateButton
              onTouchTap={e => this.onTouchCreate(e)}
            />
          </div>
        </div>
      );
    }
    return (
      <div className="row center-xs" style={rowPaddingStyle}>
        <div className="col-xs">
          <KeypairButton onTouchTap={() => this.props.routerActions.push('/keypass')} />
        </div>
      </div>
    );
  }

  isReady() {
    return this.props.account &&
      this.props.account &&
      this.props.account.publicKey &&
      this.props.privateKey;
  }

  render() {
    const rowPaddingStyle = { paddingTop: 10, paddingMargin: 10 };
    const creationBtn = this.creationComponent();
    return (
      <div className="box">
        <div className="row center-xs" style={rowPaddingStyle}>
          <div className="col-xs-12 col-sm-10 col-md-6 col-lg-4">
            <PasswordSearchField
              onChange={v => this.onQueryChange(v)}
              query={this.state.query}
            />
          </div>
        </div>
        {creationBtn}
        {this.isReady() ? (
          <div className="row center-xs" style={rowPaddingStyle}>
            <div className="col-xs-12 col-sm-10 col-md-6 col-lg-4">
              <PasswordList
                account={this.props.account}
                list={this.props.passwords}
                onDelete={p => this.onDeletePassword(p)}
                onSelect={p => this.onSelectPassword(p)}
                onShare={p => this.onSharePassword(p)}
                privateKey={this.props.privateKey.content}
                query={this.state.query}
              />
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

export default connect(
  state => ({
    account: state.account[state.authentication.userId],
    authentication: state.authentication,
    passwords: lodash(state.password)
      .filter({ ownerId: state.authentication.userId })
      .map(password => ({
        ...password,
        cipherKeys: lodash.filter(state.cipherKey, {
          passwordId: password.id,
        }),
      }))
      .value(),
    privateKey: state.privateKey,
  }),
  dispatch => ({
    accountEffect: bindActionCreators(AccountEffect, dispatch),
    routerActions: bindActionCreators({ push }, dispatch),
  }),
)(HomeView);
