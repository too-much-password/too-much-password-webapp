/* eslint no-bitwise: 0 */
import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import lodash from 'lodash';

import PasswordDisplayCard from '../components/password-display-card';

import logger from '../services/logger';

class PasswordShowView extends Component {

  static propTypes = {
    cipherKey: PropTypes.shape().isRequired,
    password: PropTypes.shape().isRequired,
    privateKey: PropTypes.shape({
      content: PropTypes.string.isRequired,
    }).isRequired,
    routerActions: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidMount() {
    logger.info('password displayed');
  }

  onCancel = () => {
    this.props.routerActions.push('/');
  }

  render() {
    return (
      <div>
        <div className="row center-xs">
          <div className="col-xs-12 col-sm-10 col-md-6 col-lg-4">
            <PasswordDisplayCard
              cipherKey={this.props.cipherKey}
              onCancel={this.onCancel}
              password={this.props.password}
              privateKey={this.props.privateKey.content}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, args) => ({
  cipherKey: lodash.find(state.cipherKey, {
    accountId: state.authentication.userId,
    passwordId: ~~args.params.id,
  }),
  privateKey: state.privateKey,
});

const mapDispatchToProps = dispatch => ({
  routerActions: bindActionCreators({ push }, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PasswordShowView);
