/* eslint no-bitwise: 0 */
import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
import { find, pick } from 'lodash';

import AccountEffect from '../effects/account';
import PasswordEffect from '../effects/password';

class PasswordRootView extends Component {

  componentWillMount() {
    if (!this.props.cipherKey) {
      this.props.passwordEffect
        .cipherKeys$find(this.props.passwordId);
    }
    if (!this.props.password) {
      this.props.accountEffect
        .passwords$find(this.props.authentication.userId);
    }
  }

  render() {
    if (!this.props.password) return <div />;
    return React.cloneElement(this.props.children, pick(this.props, [
      'authentication',
      'password',
      'passwordId',
    ]));
  }
}

PasswordRootView.propTypes = {
  accountEffect: PropTypes.shape({
    cipherKeys$find: PropTypes.func.isRequired,
    passwords$find: PropTypes.func.isRequired,
  }).isRequired,
  authentication: PropTypes.shape({
    userId: PropTypes.number.isRequired,
  }).isRequired,
  children: PropTypes.shape(),
  cipherKey: PropTypes.shape(),
  password: PropTypes.shape(),
  passwordId: PropTypes.number.isRequired,
  passwordEffect: PropTypes.shape({
    cipherKeys$find: PropTypes.func.isRequired,
  }).isRequired,
};

const mapStateToProps = (state, args) => ({
  authentication: state.authentication,
  password: find(state.password, { id: ~~args.routeParams.id }),
  passwordId: ~~args.routeParams.id,
  cipherKey: find(state.cipherKey, {
    passwordId: ~~args.routeParams.id,
    accountId: state.authentication.userId,
  }),
});

const mapDispatchToProps = dispatch => ({
  accountEffect: bindActionCreators(AccountEffect, dispatch),
  passwordEffect: bindActionCreators(PasswordEffect, dispatch),
  routerActions: bindActionCreators({ push }, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PasswordRootView);
