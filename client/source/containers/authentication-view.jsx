import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import { has } from 'lodash';

import Authentication from '../components/authentication';
import Notification from '../components/notification';

import AccountEffect from '../effects/account';
import * as NotificationActions from '../actions/notification';

import logger from '../services/logger';

const handleErrorMessage = (namespace, err) => {
  logger.error(err);
  if (has(err, 'data.error.status')) {
    const error = err.data.error;
    if (error.status === 422) {
      if (has(error, 'details.codes.email')) {
        if (error.details.codes.email.indexOf('uniqueness') >= 0) {
          return { key: 'account.email-uniqueness' };
        }
      }
    } else if (error.status === 401) {
      if (has(error, 'code') && error.code === 'LOGIN_FAILED') {
        return { key: 'authentication.invalid-credentials' };
      }
    }
  }
  return { key: `authentication.${namespace}.error` };
};


class AuthenticationView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      credentials: {
        email: null,
        password: null,
      },
    };
  }

  componentWillMount() {
    if (this.props.authentication) {
      this.props.routerActions.push('/');
    }
  }


  onLogin(e, idents) {
    return this.props.accountEffect
      .login(idents)
      .then(() => logger.info('user authenticated'))
      .then(() => this.props.routerActions.push('/'))
      .catch(err => this.props.notificationActions
          .set({ message: handleErrorMessage('login', err) }));
  }

  onRegister(e, idents) {
    return this.props.accountEffect
      .create(idents)
      .then(() => logger.info('user registered'))
      .then(() => this.onLogin(e, idents))
      .catch(err => this.props.notificationActions
          .set({ message: handleErrorMessage('register', err) }));
  }

  render() {
    return (
      <div className="box">
        <div className="row middle-xs center-xs">
          <div className="col-xs-12 col-md-6 col-lg-4">
            <Authentication
              credentials={this.state.credentials}
              onChange={credentials => this.setState({ credentials })}
              onLogin={e => this.onLogin(e, this.state.credentials)}
              onRegister={e => this.onRegister(e, this.state.credentials)}
            />
          </div>
        </div>
        <Notification />
      </div>
    );
  }
}

AuthenticationView.propTypes = {
  authentication: PropTypes.shape(),
  accountEffect: PropTypes.shape({
    create: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired,
  }).isRequired,
  notificationActions: PropTypes.shape({
    set: PropTypes.func.isRequired,
  }).isRequired,
  routerActions: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }),
};

function mapStateToProps(state) {
  return {
    authentication: state.authentication,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    accountEffect: bindActionCreators(AccountEffect, dispatch),
    notificationActions: bindActionCreators(NotificationActions, dispatch),
    routerActions: bindActionCreators({ push }, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AuthenticationView);
