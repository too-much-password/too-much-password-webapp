import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import AccountEffect from '../effects/account';
import HeaderBar from '../components/header-bar';
import Notification from '../components/notification';

class Root extends Component {

  static style = {
    container: {
      padding: 10,
    },
  };

  static propTypes = {
    account: PropTypes.shape(),
    accountEffect: PropTypes.shape({
      findOne: PropTypes.func.isRequired,
      logout: PropTypes.func.isRequired,
    }).isRequired,
    authentication: PropTypes.shape(),
    children: PropTypes.node,
    routerActions: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentWillMount() {
    if (!this.props.authentication) {
      return this.props.routerActions.push('/authentication');
    }
    if (!this.props.account) {
      return this.props.accountEffect.findOne(this.props.authentication.userId);
    }
    return null;
  }

  onRedirect = path =>
    this.props.routerActions.push(path);

  onLogout = () =>
    this.props.accountEffect.logout()
      .then(() => this.props.routerActions.push('/authentication'));

  render() {
    if (!this.props.authentication || !this.props.account) return (<div />);
    return (
      <div>
        <HeaderBar
          onLogout={this.onLogout}
          onRedirect={this.onRedirect}
        />
        <div style={Root.style.container}>
          {this.props.children}
        </div>
        <Notification />
      </div>
    );
  }

}

export default connect(
  state => ({
    account: state.authentication ? state.account[state.authentication.userId] : null,
    authentication: state.authentication,
  }),
  dispatch => ({
    accountEffect: bindActionCreators(AccountEffect, dispatch),
    routerActions: bindActionCreators({ push }, dispatch),
  }),
)(Root);
