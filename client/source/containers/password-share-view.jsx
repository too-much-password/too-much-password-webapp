/* eslint no-bitwise: 0 */
import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import lodash, { find } from 'lodash';

import crypto from 'too-much-password-crypto';
import AccountEffect from '../effects/account';
import PasswordEffect from '../effects/password';

import ShareField from '../components/password-share-field';
import SharedList from '../components/password-shared-list';

import logger from '../services/logger';

class PasswordShareView extends Component {

  static propTypes = {
    accountEffect: PropTypes.shape({
      search: PropTypes.func.isRequired,
    }).isRequired,
    authentication: PropTypes.shape({
      userId: PropTypes.number.isRequired,
    }).isRequired,
    cipherKeys: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
    })).isRequired,
    password: PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired,
    passwordId: PropTypes.number.isRequired,
    passwordEffect: PropTypes.shape({
      cipherKeys$create: PropTypes.func.isRequired,
      cipherKeys$delete: PropTypes.func.isRequired,
      cipherKeys$find: PropTypes.func.isRequired,
    }).isRequired,
    privateKey: PropTypes.shape({
      content: PropTypes.string.isRequired,
    }).isRequired,
  };

  constructor(props) {
    super(props);
    this.searchTimeout = null;
    this.state = {
      searching: false,
      query: '',
      found: null,
    };
  }

  componentWillMount() {
    this.props.passwordEffect
      .cipherKeys$find(this.props.passwordId);
  }

  onAppend = () => {
    const exist = find(this.props.cipherKeys, {
      passwordId: this.props.password.id,
      accountId: this.state.found.id,
    });
    if (exist) return this.setState({ query: '', found: null });
    const personalCipherKey = find(this.props.cipherKeys, {
      passwordId: this.props.password.id,
      accountId: this.props.authentication.userId,
    });
    return crypto.asymetric
      .decrypt(this.props.privateKey.content, personalCipherKey.content)
      .then(clearCipherKey => crypto.asymetric.encrypt(this.state.found.publicKey, clearCipherKey))
      .then(encryptedCipherKey => this.props.passwordEffect
        .cipherKeys$create(this.props.password.id, {
          accountId: this.state.found.id,
          content: encryptedCipherKey,
        }))
      .then(() => this.setState({ query: '', found: null }))
      .then(() => logger.info('password shared'))
      .catch(err => logger.error(err));
  }

  onChange = (query) => {
    this.setState({ query }, () => {
      if (this.searchTimeout) {
        clearTimeout(this.searchTimeout);
        this.searchTimeout = null;
      }
      this.searchTimeout = setTimeout(() => this.onSearch(), 500);
    });
  }

  onDelete = (cipherKey) => {
    this.props.passwordEffect
      .cipherKeys$delete(this.props.password.id, cipherKey.id)
      .then(() => logger.info('password sharing deleted'))
      .catch(err => logger.error(err));
  }

  onSearch = () => {
    if (this.state.query.length <= 2) return;
    this.setState({ searching: true }, () => {
      this.props.accountEffect.search(this.state.query)
        .then(found => this.setState({ searching: false, found }))
        .catch((err) => {
          logger.error(err);
          this.setState({ searching: false, found: null });
        });
    });
  }

  render() {
    return (
      <div className="container" id="password-share-view">
        <div className="row center-xs">
          <div className="col-xs-12 col-md-6">
            <ShareField
              found={this.state.found}
              query={this.state.query}
              onAppend={this.onAppend}
              onChange={this.onChange}
              onSearch={this.onSearch}
              searching={this.state.searching}
            />
          </div>
        </div>
        <div className="row center-xs">
          <div className="col-xs-12 col-md-6 start-xs">
            <SharedList
              cipherKeys={this.props.cipherKeys}
              password={this.props.password}
              onDelete={this.onDelete}
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, args) {
  return {
    cipherKeys: lodash(state.cipherKey)
      .filter({ passwordId: ~~args.params.id })
      .map((item) => {
        if (item.account) return item;
        return {
          ...item,
          account: state.account[item.accountId],
        };
      })
      .value(),
    privateKey: state.privateKey,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    accountEffect: bindActionCreators(AccountEffect, dispatch),
    passwordEffect: bindActionCreators(PasswordEffect, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PasswordShareView);
