import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import crypto from 'too-much-password-crypto';

import PasswordEditionCard from '../components/password-edition-card';

import AccountEffect from '../effects/account';

import logger from '../services/logger';

class PasswordCreateView extends Component {

  static propTypes = {
    account: PropTypes.shape().isRequired,
    accountEffect: PropTypes.shape({
      cipherKeys$create: PropTypes.func.isRequired,
      ownedPasswords$create: PropTypes.func.isRequired,
    }).isRequired,
    authentication: PropTypes.shape({
      userId: PropTypes.number.isRequired,
    }).isRequired,
    routerActions: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  }

  constructor(props) {
    super(props);
    this.state = {
      password: {},
    };
  }

  onChange(password) {
    this.setState({ password });
  }

  onSubmit() {
    let clearCipherkey;
    let encryptedCipherkey;
    let encryptedPassword;
    return crypto.randomGenerator.string(256, 10, true)
      .then((rand) => {
        clearCipherkey = rand;
        return crypto.symetric.encrypt(rand, this.state.password.content);
      })
      .then((encrypted) => {
        encryptedPassword = encrypted;
        return crypto.asymetric.encrypt(this.props.account.publicKey, clearCipherkey);
      })
      .then((encrypted) => {
        encryptedCipherkey = encrypted;
        return this.props.accountEffect.ownedPasswords$create(
          this.props.authentication.userId,
          { ...this.state.password, content: encryptedPassword },
        );
      })
      .then(password => this.props.accountEffect
        .cipherKeys$create(
          this.props.authentication.userId,
          { passwordId: password.id, content: encryptedCipherkey },
        ))
      .then(() => this.props.routerActions.push('/'))
      .catch(err => logger.error(err));
  }

  onCancel() {
    this.props.routerActions.push('/');
  }

  render() {
    return (
      <div>
        <div className="row center-xs">
          <div className="col-xs-12 col-sm-10 col-md-6 col-lg-4">
            <PasswordEditionCard
              onCancel={() => this.onCancel()}
              onChange={value => this.onChange(value)}
              onSubmit={() => this.onSubmit()}
              password={this.state.password}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    account: state.account,
    authentication: state.authentication,
    password: state.password,
    privateKey: state.privateKey,
  }),
  dispatch => ({
    accountEffect: bindActionCreators(AccountEffect, dispatch),
    routerActions: bindActionCreators({ push }, dispatch),
  }),
)(PasswordCreateView);
