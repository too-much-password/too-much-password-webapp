import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import AccountEffect from '../effects/account';
import PrivateKeyAction from '../actions/private-key';

import KeypairCard from '../components/account-keypair-card';

import logger from '../services/logger';

class KeypassView extends Component {

  static propTypes = {
    account: PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired,
    accountEffect: PropTypes.shape({
      update: PropTypes.func.isRequired,
    }).isRequired,
    authentication: PropTypes.shape().isRequired,
    privateKey: PropTypes.shape({
      content: PropTypes.string,
      persist: PropTypes.bool,
    }),
    privateKeyAction: PropTypes.shape({
      update: PropTypes.func.isRequired,
    }).isRequired,
    routerActions: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
  };

  componentWillMount() {
    if (!this.props.authentication) {
      this.props.routerActions.push('/authentication');
    }
  }

  onPublicKeyChange = publicKey =>
    this.props.accountEffect
      .update(this.props.authentication.userId, { publicKey })
      .catch(err => logger.error(err));

  onPrivateKeyChange = privateKey =>
    this.props.privateKeyAction.update({ content: privateKey });

  onPrivateKeyChangePersistence = enabled =>
    this.props.privateKeyAction.update({ persist: enabled });

  onKeypairGenerated = keypair =>
    this.onPublicKeyChange(keypair.publicKey)
      .then(() => this.onPrivateKeyChange(keypair.privateKey))
      .then(() => logger.info('keypair generated'))
      .catch(err => logger.error(err));

  render() {
    return (
      <div className="box">
        <div className="row center-xs">
          <div className="col-xs-12 col-md-6 col-lg-4">
            <KeypairCard
              account={this.props.account}
              onGenerated={this.onKeypairGenerated}
              onPrivateKeyChange={this.onPrivateKeyChange}
              onPrivateKeyChangePersistence={this.onPrivateKeyChangePersistence}
              onPublicKeyChange={this.onPublicKeyChange}
              persistPrivateKey={this.props.privateKey.persist}
              privateKey={this.props.privateKey.content}
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    account: state.authentication ? state.account[state.authentication.userId] : null,
    authentication: state.authentication,
    privateKey: state.privateKey,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    accountEffect: bindActionCreators(AccountEffect, dispatch),
    privateKeyAction: bindActionCreators(PrivateKeyAction, dispatch),
    routerActions: bindActionCreators({ push }, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(KeypassView);
