import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';

import PasswordCard from '../components/account-password-card';

import AccountEffect from '../effects/account';

class PasswordView extends Component {

  static propTypes = {
    accountEffect: PropTypes.shape({
      update: PropTypes.func.isRequired,
    }).isRequired,
    authentication: PropTypes.shape().isRequired,
    routerActions: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
  };

  constructor(props) {
    super(props);
    this.state = {
      credentials: {},
    };
  }

  componentWillMount() {
    if (!this.props.authentication) {
      this.props.routerActions.push('/authentication');
    }
  }

  onCancel = () =>
    this.props.routerActions.push('/');

  onChange(credentials) {
    this.setState({ credentials });
  }

  onSubmit = () =>
    this.props.accountEffect
      .update(this.props.authentication.userId, {
        password: this.state.credentials.newPassword,
      })
      .then(() => this.onCancel());

  render() {
    return (
      <div className="box">
        <div className="row center-xs">
          <div className="col-xs-12 col-md-6 col-lg-4">
            <PasswordCard
              credentials={this.state.credentials}
              onCancel={this.onCancel}
              onChange={(e, value) => this.onChange(value)}
              onSubmit={this.onSubmit}
            />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    authentication: state.authentication,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    accountEffect: bindActionCreators(AccountEffect, dispatch),
    routerActions: bindActionCreators({ push }, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PasswordView);
