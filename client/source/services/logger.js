import RollbarBrowser from 'rollbar-browser';

const createLogger = () => {
  if (process.env.ROLLBAR_ENABLED) {
    RollbarBrowser.init({
      accessToken: process.env.ROLLBAR_ACCESSTOKEN,
      captureUncaught: true,
      captureUnhandledRejections: true,
      payload: {
        environment: process.env.NODE_ENV || 'development',
      },
    });
    return window.Rollbar;
  }
  return console;
};

export default createLogger();
