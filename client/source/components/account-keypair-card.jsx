import React, { Component, PropTypes } from 'react';
import Avatar from 'material-ui/Avatar';
import { Card } from 'material-ui/Card';
import FontIcon from 'material-ui/FontIcon';
import { List, ListItem } from 'material-ui/List';
import { blue500 } from 'material-ui/styles/colors';
import Translate from 'react-translate-component';
import { asymetric } from 'too-much-password-crypto';

import KeyListItem from './account-keypair-list-item';

import logger from '../services/logger';


export default class KeypairCard extends Component {

  constructor(props) {
    super(props);
    this.state = { working: false };
  }

  onGenerate() {
    this.setState({ working: true });
    return Promise.resolve(asymetric.keygen(this.props.account))
      .then(this.props.onGenerated)
      .then(() => this.setState({ working: false }))
      .catch(err => logger.error(err));
  }

  render() {
    return (
      <div className="box start-xs">
        <Card>
          <List>
            <KeyListItem
              {...this.props}
              id="keypaircard_item_public"
              name="public"
              onChange={this.props.onPublicKeyChange}
              value={this.props.account ? this.props.account.publicKey : null}
            />
            <KeyListItem
              {...this.props}
              id="keypaircard_item_private"
              name="private"
              onChange={this.props.onPrivateKeyChange}
              onChangePersistence={this.props.onPrivateKeyChangePersistence}
              persist={this.props.persistPrivateKey}
              persistable
              value={this.props.privateKey}
            />
            <ListItem
              disabled={this.state.working}
              id="keypaircard_item_generate"
              leftAvatar={
                <Avatar
                  backgroundColor={blue500}
                  icon={<FontIcon className="material-icons">refresh</FontIcon>}
                />
              }
              onTouchTap={e => this.onGenerate(e)}
              primaryText={<Translate content="account.keypair-generate" />}
            />
          </List>
        </Card>
      </div>
    );
  }

}

KeypairCard.propTypes = {
  account: PropTypes.shape({
    publicKey: PropTypes.string,
  }),
  onGenerated: PropTypes.func.isRequired,
  onPrivateKeyChange: PropTypes.func.isRequired,
  onPrivateKeyChangePersistence: PropTypes.func.isRequired,
  onPublicKeyChange: PropTypes.func.isRequired,
  persistPrivateKey: PropTypes.bool,
  privateKey: PropTypes.string,
};
