import React, { Component, PropTypes } from 'react';

import { Card, CardActions, CardTitle, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import counterpart from 'counterpart';
import validator from 'validator';

class Authentication extends Component {

  onFormChange(field, value) {
    this.props.onChange(Object.assign({}, this.props.credentials, { [field]: value }));
  }

  isFormValid() {
    return this.props.credentials &&
      this.props.credentials.email &&
      validator.isEmail(this.props.credentials.email) &&
      this.props.credentials.password &&
      validator.isLength(this.props.credentials.password, { min: 6 });
  }

  render() {
    return (
      <div className="box">
        <Card>
          <CardTitle title={counterpart('authentication.title')} />
          <CardText>
            <div className="row">
              <div className="col-xs-12">
                <TextField
                  floatingLabelText={counterpart('account.email')}
                  fullWidth
                  id="authentication_input_email"
                  onChange={e => this.onFormChange('email', e.target.value)}
                  // eslint-disable-next-line jsx-a11y/tabindex-no-positive
                  tabIndex={1}
                  type="email"
                  value={this.props.credentials.email || ''}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-xs-12">
                <TextField
                  floatingLabelText={counterpart('account.password')}
                  fullWidth
                  id="authentication_input_password"
                  onChange={e => this.onFormChange('password', e.target.value)}
                  // eslint-disable-next-line jsx-a11y/tabindex-no-positive
                  tabIndex={2}
                  type="password"
                  value={this.props.credentials.password || ''}
                />
              </div>
            </div>
          </CardText>
          <CardActions>
            <div className="row center-xs">
              <div className="col-xs-6">
                <RaisedButton
                  disabled={!this.isFormValid()}
                  id="authentication_btn_login"
                  label={counterpart('authentication.login.button')}
                  onTouchTap={this.props.onLogin}
                  primary
                  // eslint-disable-next-line jsx-a11y/tabindex-no-positive
                  tabIndex={3}
                />
              </div>
              <div className="col-xs-6">
                <RaisedButton
                  disabled={!this.isFormValid()}
                  id="authentication_btn_register"
                  label={counterpart('authentication.register.button')}
                  onTouchTap={this.props.onRegister}
                  // eslint-disable-next-line jsx-a11y/tabindex-no-positive
                  tabIndex={4}
                />
              </div>
            </div>
          </CardActions>
        </Card>
      </div>
    );
  }
}

Authentication.propTypes = {
  credentials: PropTypes.shape({
    email: PropTypes.string,
    password: PropTypes.string,
  }).isRequired,
  onChange: PropTypes.func.isRequired,
  onLogin: PropTypes.func.isRequired,
  onRegister: PropTypes.func.isRequired,
};

export default Authentication;
