import React, { Component, PropTypes } from 'react';
import moment from 'moment';

import { find } from 'lodash';

import Avatar from 'material-ui/Avatar';
import { ListItem } from 'material-ui/List';
import FontIcon from 'material-ui/FontIcon';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton/IconButton';
import { green500, green300, orange500 } from 'material-ui/styles/colors';

import counterpart from 'counterpart';

class PasswordListItem extends Component {

  isReadable() {
    return this.props.account && this.props.account.id &&
      find(this.props.password.cipherKeys, { accountId: this.props.account.id });
  }

  isEditable() {
    return this.props.account && this.props.account.id
      && this.props.account.id === this.props.password.ownerId;
  }

  avatarColor() {
    if (!this.isReadable()) {
      return orange500;
    }
    if (this.isEditable()) {
      return green500;
    }
    return green300;
  }

  secondaryText() {
    return moment(this.props.password.createdAt).format('LLL');
  }

  render() {
    const readable = this.isReadable();
    const menuStyle = { horizontal: 'right', vertical: 'top' };
    let btnMenu;
    let rightMenu;
    if (this.isEditable()) {
      btnMenu = (
        <IconButton className="password-list-item_menu-btn">
          <FontIcon className="material-icons">more_vert</FontIcon>
        </IconButton>
      );
      rightMenu = (
        <IconMenu
          anchorOrigin={menuStyle}
          iconButtonElement={btnMenu}
          targetOrigin={menuStyle}
        >
          <MenuItem
            className="password-list-item_btn-share"
            leftIcon={<FontIcon className="material-icons">share</FontIcon>}
            onTouchTap={this.props.onShare}
            primaryText={counterpart('password.share')}
          />
          <MenuItem
            className="password-list-item_btn-delete"
            leftIcon={<FontIcon className="material-icons">delete</FontIcon>}
            onTouchTap={this.props.onDelete}
            primaryText={counterpart('password.delete')}
          />
        </IconMenu>
      );
    }
    const leftAvatar = (
      <Avatar
        backgroundColor={this.avatarColor()}
        icon={<FontIcon className="material-icons">vpn_key</FontIcon>}
      />
    );
    return (
      <ListItem
        className="password-list-item"
        disabled={!readable}
        leftAvatar={leftAvatar}
        onTouchTap={this.props.onSelect}
        primaryText={this.props.password.title}
        rightIconButton={rightMenu}
        secondaryText={this.secondaryText()}
      />
    );
  }

}

PasswordListItem.propTypes = {
  account: PropTypes.shape({
    id: PropTypes.number.isRequired,
  }).isRequired,
  onDelete: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  onShare: PropTypes.func.isRequired,
  password: PropTypes.shape({
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    cipherKeys: PropTypes.arrayOf(PropTypes.object).isRequired,
    ownerId: PropTypes.number.isRequired,
  }).isRequired,
};

export default PasswordListItem;
