import React, { Component, PropTypes } from 'react';

import AppBar from 'material-ui/AppBar';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import MenuItem from 'material-ui/MenuItem';

import counterpart from 'counterpart';

export default class HeaderBar extends Component {

  onTouchTitle() {
    this.props.onRedirect('/');
  }

  onTouchAccountKeypass() {
    this.props.onRedirect('/keypass');
  }

  onTouchAccountPassword() {
    this.props.onRedirect('/change-password');
  }

  render() {
    const style = { margin: 0 };
    const menuAnchor = { horizontal: 'right', vertical: 'top' };
    const targetAnchor = { horizontal: 'right', vertical: 'top' };
    return (
      <AppBar
        className="row"
        iconElementLeft={
          <IconButton
            iconClassName="material-icons"
            onTouchTap={e => this.onTouchTitle(e)}
          >home</IconButton>
        }
        iconElementRight={
          <IconMenu
            anchorOrigin={menuAnchor}
            iconButtonElement={
              <IconButton
                iconClassName="material-icons"
              >person</IconButton>
            }
            targetOrigin={targetAnchor}
          >
            <MenuItem
              onTouchTap={e => this.onTouchAccountKeypass(e)}
              primaryText={counterpart('account.keypass')}
            />
            <MenuItem
              onTouchTap={e => this.onTouchAccountPassword(e)}
              primaryText={counterpart('account.password')}
            />
            <MenuItem
              onTouchTap={this.props.onLogout}
              primaryText={counterpart('account.log-out')}
            />
          </IconMenu>
        }
        id="header-bar"
        onTitleTouchTap={e => this.onTouchTitle(e)}
        style={style}
        title="Too much password !"
      />
    );
  }

}

HeaderBar.propTypes = {
  onLogout: PropTypes.func.isRequired,
  onRedirect: PropTypes.func.isRequired,
};
