import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Snackbar from 'material-ui/Snackbar';
import counterpart from 'counterpart';
import _ from 'lodash';

import * as NotificationActions from '../actions/notification';

class Notification extends Component {

  handleRequestClose() {
    this.props.notificationActions.unset();
  }

  render() {
    const { isOpen, timeout, message } = this.props.notificationStatus;
    let msg;
    if (_.isString(message)) {
      msg = message;
    } else if (_.has(message, 'key')) {
      msg = counterpart(message.key);
    }
    return (
      <Snackbar
        action="Close"
        autoHideDuration={timeout}
        message={msg}
        onActionTouchTap={e => this.handleRequestClose(e)}
        onRequestClose={e => this.handleRequestClose(e)}
        open={isOpen}
      />
    );
  }
}

Notification.propTypes = {
  notificationActions: PropTypes.shape({
    unset: PropTypes.func.isRequired,
  }).isRequired,
  notificationStatus: PropTypes.shape({
    isOpen: PropTypes.bool.isRequired,
    timeout: PropTypes.number.isRequired,
    message: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.string,
    ]),
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    notificationStatus: state.notification,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    notificationActions: bindActionCreators(NotificationActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Notification);
