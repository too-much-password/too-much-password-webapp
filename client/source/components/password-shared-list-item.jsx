import React, { PropTypes } from 'react';
import { ListItem } from 'material-ui/List';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import { lightBlack } from 'material-ui/styles/colors';

import Translate from 'react-translate-component';

const accountToPrimaryText = account => account.email;

const ownerIcon = (<FontIcon className="material-icons">person</FontIcon>);
const ownerLabel = (<Translate content="password.owner" />);

const rightButton = props => (
  <IconButton onTouchTap={props.onDelete}>
    <FontIcon
      className="material-icons"
      color={lightBlack}
    >delete</FontIcon>
  </IconButton>
);

rightButton.propTypes = {
  onDelete: PropTypes.func.isRequired,
};

const SharedListItem = props => (
  <ListItem
    className="password-shared-list-item"
    disabled
    insetChildren={!props.isOwner}
    primaryText={accountToPrimaryText(props.cipherKey.account)}
    secondaryText={props.isOwner ? ownerLabel : null}
    leftIcon={props.isOwner ? ownerIcon : null}
    rightIconButton={props.isOwner ? null : rightButton(props)}
  />
);

SharedListItem.propTypes = {
  cipherKey: PropTypes.shape({
    account: PropTypes.object.isRequired,
  }).isRequired,
  isOwner: PropTypes.bool.isRequired,
};

export default SharedListItem;
