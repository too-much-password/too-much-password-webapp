import React, { Component, PropTypes } from 'react';

import Checkbox from 'material-ui/Checkbox';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
import Subheader from 'material-ui/Subheader';

import Translate from 'react-translate-component';
import counterpart from 'counterpart';

import { passwordGenerator } from 'too-much-password-crypto';

const checkboxLabel = item => counterpart(`password.generation.${item}`);

export default class PasswordGenerationPanel extends Component {

  constructor(props) {
    super(props);
    this.state = {
      letters: true,
      numbers: true,
      symbols: true,
      length: 8,
    };
  }

  generate() {
    if (!this.props.onGenerate) return;
    passwordGenerator
      .generate({
        letters: this.state.letters,
        numbers: this.state.numbers,
        symbols: this.state.symbols,
        length: this.state.length,
      })
      .then(result => this.props.onGenerate(result));
  }

  render() {
    const checkboxes = ['letters', 'numbers', 'symbols'].map((item, i) => (
      <div className="col-xs-12 col-md-4" key={i}>
        <Checkbox
          checked={this.state[item]}
          label={checkboxLabel(item)}
          name={item}
          onCheck={(e, value) => this.setState({ [item]: value })}
        />
      </div>
    ));
    return (
      <div>
        <div className="row">
          <Subheader>
            <Translate content="password.generation.title" />
          </Subheader>
        </div>
        <div className="row">{checkboxes}</div>
        <div className="row bottom-xs">
          <div className="col-xs">
            <TextField
              floatingLabelText={counterpart('password.generation.length')}
              fullWidth
              onChange={e => this.setState({ length: e.target.value })}
              type="number"
              value={this.state.length}
            />
          </div>
          <div>
            <IconButton
              iconClassName="material-icons"
              onTouchTap={() => this.generate()}
            >autorenew</IconButton>
          </div>
        </div>
      </div>
    );
  }

}

PasswordGenerationPanel.propTypes = {
  onGenerate: PropTypes.func.isRequired,
};
