import React, { PropTypes } from 'react';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import counterpart from 'counterpart';

const vpnIcon = (<FontIcon className="material-icons">vpn_key</FontIcon>);

const KeypairButton = props => (
  <RaisedButton
    icon={vpnIcon}
    id="keypair_btn"
    label={counterpart('keypair.configuration')}
    onTouchTap={props.onTouchTap}
    secondary
  />
);

KeypairButton.propTypes = {
  onTouchTap: PropTypes.func,
};

export default KeypairButton;
