import React, { Component, PropTypes } from 'react';
import { Card } from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import { List } from 'material-ui/List';
import lunr from 'lunr';
import _ from 'lodash';
import PasswordListItem from './password-list-item';

export default class PasswordList extends Component {

  constructor(props) {
    super(props);
    this.indexation = function indexation() {
      this.ref('id');
      this.field('title', 100);
      this.field('username', 50);
      this.field('url', 50);
    };
  }

  results() {
    const index = lunr(this.indexation);
    this.props.list.forEach(item => index.add(item));
    if (this.props.query) {
      const result = index.search(this.props.query);
      return _.reduce(result, (db, item) => {
        const found = _.find(this.props.list, { id: item.ref });
        return found ? [...db, found] : db;
      }, []);
    }
    return this.props.list;
  }

  resultItems() {
    return this.results().map((item, i) => (
      <div key={i}>
        {i > 0 ? <Divider inset /> : null}
        <PasswordListItem
          account={this.props.account}
          onDelete={() => this.props.onDelete(item)}
          onSelect={() => this.props.onSelect(item)}
          onShare={() => this.props.onShare(item)}
          password={item}
        />
      </div>
    ));
  }

  render() {
    const results = this.resultItems();
    if (results.length === 0) return <div />;
    return (
      <div className="box start-xs">
        <Card>
          <List>
            {results}
          </List>
        </Card>
      </div>
    );
  }

}

PasswordList.propTypes = {
  account: PropTypes.shape({}).isRequired,
  list: PropTypes.arrayOf(PropTypes.object).isRequired,
  onDelete: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
  onShare: PropTypes.func.isRequired,
  query: PropTypes.string,
};
