import React, { Component, PropTypes } from 'react';

import { Card, CardActions, CardText } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import Translate from 'react-translate-component';
import validator from 'validator';

import PasswordGenerationPanel from './password-generation-panel';

export default class PasswordEditionCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      generation: false,
    };
  }

  onChange(field, value) {
    this.props.onChange({ ...this.props.password, [field]: value });
  }

  onSubmit(e) {
    this.props.onSubmit(e);
  }

  isValid() {
    return !!this.props.password.title &&
      validator.isLength(this.props.password.title, { min: 2 }) &&
      (!this.props.password.url || validator.isURL(this.props.password.url)) &&
      !!this.props.password.content;
  }

  render() {
    const btnStyle = { marginTop: 26 };
    const expandIcon = this.state.generation ? 'expand_less' : 'expand_more';
    const visibilityIcon = this.state.visible ? 'visibility' : 'visibility_off';
    const visibilityType = this.state.visible ? 'text' : 'password';
    return (
      <Card>
        <CardText>
          <div className="row">
            <div className="col-xs-12">
              <TextField
                autoFocus
                className="password-edition-dialog_title"
                floatingLabelText={<Translate content="password.title" />}
                fullWidth
                name="title"
                onChange={e => this.onChange('title', e.target.value)}
                // eslint-disable-next-line jsx-a11y/tabindex-no-positive
                tabIndex={1}
                value={this.props.password.title || ''}
              />
            </div>
            <div className="col-xs-12 col-md-6">
              <TextField
                className="password-edition-dialog_username"
                floatingLabelText={<Translate content="password.username" />}
                fullWidth
                name="username"
                onChange={e => this.onChange('username', e.target.value)}
                // eslint-disable-next-line jsx-a11y/tabindex-no-positive
                tabIndex={2}
                value={this.props.password.username || ''}
              />
            </div>
            <div className="col-xs-12 col-md-6">
              <TextField
                className="password-edition-dialog_url"
                floatingLabelText={<Translate content="password.url" />}
                fullWidth
                name="url"
                onChange={e => this.onChange('url', e.target.value)}
                // eslint-disable-next-line jsx-a11y/tabindex-no-positive
                tabIndex={3}
                value={this.props.password.url || ''}
              />
            </div>
          </div>
          <div className="row">
            <div>
              <IconButton
                iconClassName="material-icons"
                onTouchTap={() => this.setState({ visible: !this.state.visible })}
                style={btnStyle}
              >{visibilityIcon}</IconButton>
            </div>
            <div className="col-xs">
              <TextField
                className="password-edition-dialog_content"
                floatingLabelText={<Translate content="password.content" />}
                fullWidth
                name="content"
                onChange={e => this.onChange('content', e.target.value)}
                // eslint-disable-next-line jsx-a11y/tabindex-no-positive
                tabIndex={4}
                type={visibilityType}
                value={this.props.password.content || ''}
              />
            </div>
            <div>
              <IconButton
                iconClassName="material-icons"
                onTouchTap={() => this.setState({ generation: !this.state.generation })}
                style={btnStyle}
              >{expandIcon}</IconButton>
            </div>
          </div>
          {this.state.generation ? (
            <PasswordGenerationPanel
              onGenerate={result => this.onChange('content', result)}
            />
          ) : null}
        </CardText>
        <CardActions className="end-xs">
          <RaisedButton
            className="password-edition-dialog_cancel"
            label={<Translate content="password.cancel" />}
            onTouchTap={this.props.onCancel}
          />
          <RaisedButton
            className="password-edition-dialog_submit"
            label={<Translate content="password.save" />}
            onTouchTap={this.props.onSubmit}
            disabled={!this.isValid()}
            primary
            // eslint-disable-next-line jsx-a11y/tabindex-no-positive
            tabIndex={5}
          />
        </CardActions>
      </Card>
    );
  }

}

PasswordEditionCard.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  password: PropTypes.shape({
    title: PropTypes.string,
    username: PropTypes.string,
    url: PropTypes.string,
    content: PropTypes.string,
  }).isRequired,
};
