import React, { Component, PropTypes } from 'react';

import CircularProgress from 'material-ui/CircularProgress';
import { Card, CardText, CardTitle, CardActions } from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import counterpart from 'counterpart';
import Translate from 'react-translate-component';
import crypto from 'too-much-password-crypto';

export default class PasswordDisplayCard extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      value: null,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      crypto.asymetric
        .decrypt(this.props.privateKey, this.props.cipherKey.content)
        .then(cipherKey => crypto.symetric.decrypt(cipherKey, this.props.password.content))
        .then(password => this.setState({ value: password, loading: false }))
        .catch(alert);
    }, 500);
  }

  render() {
    return (
      <Card>
        <CardTitle title={counterpart('password.display.title')} subtitle="Card subtitle" />
        <CardText>
          <div className="row center-xs" id="password-display-dialog_content">
            <div className="col-xs-12">
              {this.state.loading ? (
                <CircularProgress />
              ) : (
                <TextField
                  id="password-display-dialog_textfield"
                  fullWidth
                  name="password-display-dialog-value"
                  value={this.state.value}
                />
              )}
            </div>
          </div>
        </CardText>
        <CardActions className="end-xs">
          <RaisedButton
            className="password-display-dialog_close"
            label={<Translate content="password.close" />}
            onTouchTap={this.props.onCancel}
          />
        </CardActions>
      </Card>
    );
  }
}

PasswordDisplayCard.propTypes = {
  cipherKey: PropTypes.shape({
    content: PropTypes.string.isRequired,
  }).isRequired,
  onCancel: PropTypes.func.isRequired,
  password: PropTypes.shape({
    content: PropTypes.string,
  }).isRequired,
  privateKey: PropTypes.string.isRequired,
};
