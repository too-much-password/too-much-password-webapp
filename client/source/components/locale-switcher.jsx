import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';

import moment from 'moment';
import counterpart from 'counterpart';

import * as LanguageActions from '../actions/language';

class LocaleSwitcher extends Component {

  handleSelectLocale(event, locale) {
    this.props.languageActions.select(locale);
  }

  render() {
    const icon = (
      <IconButton>
        <FontIcon
          className="material-icons"
          color={this.props.color}
        >language</FontIcon>
      </IconButton>
    );
    const items = this.props.languageStatus.available.map((language, i) => (
      <MenuItem
        key={i}
        primaryText={language.label}
        value={language.key}
      />
    ));
    return (
      <IconMenu
        onChange={this.handleSelectLocale.bind(this)}
        value={this.props.languageStatus.selected}
        iconButtonElement={icon}
      >
        {items}
      </IconMenu>
    );
  }
}

function mapStateToProps(state) {
  return {
    languageStatus: state.language,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    languageActions: bindActionCreators(LanguageActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LocaleSwitcher);
