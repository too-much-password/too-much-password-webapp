import React, { Component, PropTypes } from 'react';
import Avatar from 'material-ui/Avatar';
import FontIcon from 'material-ui/FontIcon';
import { ListItem } from 'material-ui/List';
import IconButton from 'material-ui/IconButton/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';

import { green500, red500 } from 'material-ui/styles/colors';
import Translate from 'react-translate-component';

import Dropzone from 'react-dropzone';

const styles = {
  drop: {
    width: null,
    height: null,
    border: null,
  },
  menu: {
    horizontal: 'right',
    vertical: 'top',
  },
};

export default class KeyListItem extends Component {

  onDownload() {
    const a = document.createElement('a');
    const blob = new Blob([this.props.value], { type: 'plain/text' });
    a.href = window.URL.createObjectURL(blob);
    a.download = `${this.props.name}-key.txt`;
    a.click();
  }

  onClickUpload() {
    this.dropzone.open();
  }

  onSwitchPersist() {
    this.props.onChangePersistence(!this.props.persist);
  }

  onUpload(file) {
    const fr = new FileReader();
    fr.onloadend = () => {
      this.props.onChange(fr.result);
    };
    fr.readAsText(file[0]);
  }

  assignDropzone(zone) {
    this.dropzone = zone;
  }

  render() {
    const btnMenu = (
      <IconButton>
        <FontIcon className="material-icons">more_vert</FontIcon>
      </IconButton>
    );
    const rightMenu = (
      <IconMenu
        anchorOrigin={styles.menu}
        iconButtonElement={btnMenu}
        targetOrigin={styles.menu}
      >
        <MenuItem
          insetChildren={this.props.persistable}
          onTouchTap={() => this.onClickUpload()}
          primaryText={<Translate content="keypair.upload" />}
        />
        <MenuItem
          insetChildren={this.props.persistable}
          onTouchTap={() => this.onDownload()}
          primaryText={<Translate content="keypair.download" />}
        />
        {this.props.persistable ? (
          <MenuItem
            checked={this.props.persist}
            insetChildren={!this.props.persist}
            onTouchTap={() => this.onSwitchPersist()}
            primaryText={<Translate content="keypair.persist" />}
          />
        ) : null}
      </IconMenu>
    );
    return (
      <Dropzone
        disableClick
        onDrop={file => this.onUpload(file)}
        ref={ref => this.assignDropzone(ref)}
        style={styles.drop}
      >
        <ListItem
          disabled
          id={this.props.id}
          leftAvatar={
            <Avatar
              backgroundColor={this.props.value ? green500 : red500}
              icon={
                <FontIcon className="material-icons">{this.props.value ? 'check' : 'clear'}</FontIcon>
              }
            />
          }
          rightIconButton={rightMenu}
          primaryText={<Translate component="div" content={`account.${this.props.name}-key`} />}
          secondaryText={this.props.value ? (
            <Translate content={`account.${this.props.name}-key-found`} />
          ) : (
            <Translate content={`account.${this.props.name}-key-not-found`} />
          )}
        />
      </Dropzone>
    );
  }
}

KeyListItem.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onChangePersistence: PropTypes.func,
  persist: PropTypes.bool,
  persistable: PropTypes.bool,
  value: PropTypes.string,
};
