import React, { PropTypes } from 'react';

import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';

import counterpart from 'counterpart';

const PasswordCreateButton = props => (
  <RaisedButton
    icon={<FontIcon className="material-icons">add</FontIcon>}
    id="passwordcreatebutton"
    label={counterpart('password.create')}
    onTouchTap={props.onTouchTap}
  />
);

PasswordCreateButton.propTypes = {
  onTouchTap: PropTypes.func.isRequired,
};

export default PasswordCreateButton;
