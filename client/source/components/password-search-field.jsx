import React, { PropTypes } from 'react';

import TextField from 'material-ui/TextField';
import counterpart from 'counterpart';

const floatingLabelStyle = { color: 'white' };
const textStyle = { color: 'white' };

const PasswordSearchField = props => (
  <div className="box">
    <div className="row bottom-xs">
      <div className="col-xs">
        <TextField
          floatingLabelStyle={floatingLabelStyle}
          floatingLabelText={counterpart('password.search.query')}
          fullWidth
          id="passwordsearch_input"
          inputStyle={textStyle}
          onChange={e => props.onChange(e.target.value)}
          value={props.query || ''}
        />
      </div>
    </div>
  </div>
);

PasswordSearchField.propTypes = {
  onChange: PropTypes.func.isRequired,
  query: PropTypes.string,
};

export default PasswordSearchField;
