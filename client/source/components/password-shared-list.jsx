import React, { PropTypes } from 'react';
import { Card } from 'material-ui/Card';
import { List } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Translate from 'react-translate-component';

import SharedListItem from './password-shared-list-item';

const SharedList = props => (
  <Card className="password-shared-list">
    <List>
      <Subheader>
        <Translate content="password.allowed-users" />
      </Subheader>
      {props.cipherKeys.map(key => (
        <SharedListItem
          cipherKey={key}
          isOwner={key.accountId === props.password.ownerId}
          key={key.id}
          onDelete={() => props.onDelete(key)}
        />
      ))}
    </List>
  </Card>
);

SharedList.propTypes = {
  password: PropTypes.shape().isRequired,
  cipherKeys: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default SharedList;
