import React, { PropTypes } from 'react';
import FontIcon from 'material-ui/FontIcon';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Translate from 'react-translate-component';

const addIcon = (<FontIcon className="material-icons share-field_add-icon">add</FontIcon>);
const searchIcon = (<FontIcon className="material-icons share-field_search-icon">search</FontIcon>);
const loadingIcon = (<FontIcon className="material-icons share-field_loading-icon">refresh</FontIcon>);

const styles = {
  button: { marginTop: 26 },
  label: { color: 'white' },
  text: { color: 'white' },
};

const getButtonIcon = (props) => {
  if (props.searching) return loadingIcon;
  if (props.found) return addIcon;
  return searchIcon;
};

const getButtonFunction = (props) => {
  if (props.found) return props.onAppend;
  return props.onSearch;
};

const ShareField = props => (
  <div className="row">
    <div className="col-xs col-md">
      <TextField
        className="share-field_input"
        floatingLabelStyle={styles.label}
        floatingLabelText={<Translate content="password.sharing.query" />}
        fullWidth
        inputStyle={styles.label}
        name="share-field_input"
        value={props.query}
        onChange={e => props.onChange(e.target.value)}
      />
    </div>
    <div>
      <RaisedButton
        className="share-field_button"
        style={styles.button}
        disabled={props.searching}
        icon={getButtonIcon(props)}
        onTouchTap={getButtonFunction(props)}
        primary={!!props.found}
      />
    </div>
  </div>
);

ShareField.propTypes = {
  query: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  searching: PropTypes.bool.isRequired,
  found: PropTypes.shape({
  }),
};

export default ShareField;
