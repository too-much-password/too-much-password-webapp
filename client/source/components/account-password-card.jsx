import React, { Component, PropTypes } from 'react';
import { Card, CardActions, CardTitle, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import counterpart from 'counterpart';

export default class PasswordCard extends Component {

  onChange(field, e) {
    this.props.onChange(e, {
      ...this.props.credentials,
      [field]: e.target.value,
    });
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.onSubmit(e);
  }

  render() {
    return (
      <form className="box start-xs" onSubmit={e => this.onSubmit(e)}>
        <Card>
          <CardTitle title={counterpart('account.change-password')} />
          <CardText>
            <div className="row">
              <div className="col-xs-12">
                <TextField
                  floatingLabelText={counterpart('account.old-password')}
                  fullWidth
                  name="old-password"
                  onChange={e => this.onChange('oldPassword', e)}
                  type="password"
                  value={this.props.credentials.oldPassword || ''}
                />
              </div>
              <div className="col-xs-12">
                <TextField
                  floatingLabelText={counterpart('account.new-password')}
                  fullWidth
                  name="new-password"
                  onChange={e => this.onChange('newPassword', e)}
                  type="password"
                  value={this.props.credentials.newPassword || ''}
                />
              </div>
            </div>
          </CardText>
          <CardActions className="end-xs">
            <RaisedButton
              label={counterpart('account.cancel')}
              onTouchTap={this.props.onCancel}
            />
            <RaisedButton
              label={counterpart('account.update')}
              primary
              type="submit"
            />
          </CardActions>
        </Card>
      </form>
    );
  }

}

PasswordCard.propTypes = {
  credentials: PropTypes.shape({
    oldPassword: PropTypes.string,
    newPassword: PropTypes.string,
  }).isRequired,
  onCancel: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};
