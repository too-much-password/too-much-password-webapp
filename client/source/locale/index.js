import counterpart from 'counterpart';
import moment from 'moment';

import localeFr from './locale-fr.json';
import localeEn from './locale-en.json';

moment.locale('en');
moment.locale('fr');
counterpart.registerTranslations('en', localeEn);
counterpart.registerTranslations('fr', localeFr);
