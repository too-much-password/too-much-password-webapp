import axios from 'axios';
import lodash from 'lodash';
import actions from '../actions';

const getHeaders = (getState) => {
  const token = getState().authentication;
  if (token) {
    return { headers: { Authorization: token.id } };
  }
  return null;
};

export const generateRelation = (options, relation) => {
  const action = actions[relation.action];

  const services = {};

  services[`${relation.name}$find`] = (id, filter = {}) => (dispatch, getState) => {
    const url = `/api/${options.model}/${id}/${relation.name}?filter=${encodeURIComponent(JSON.stringify(filter))}`;
    return axios.get(url, getHeaders(getState))
      .then((res) => {
        dispatch(action.createList(res.data));
        return res.data;
      });
  };

  services[`${relation.name}$create`] = (id, data) => (dispatch, getState) => {
    const url = `/api/${options.model}/${id}/${relation.name}`;
    return axios.post(url, data, getHeaders(getState))
      .then((res) => {
        dispatch(action.create(res.data));
        return res.data;
      });
  };

  services[`${relation.name}$update`] = (id, fk, data) => (dispatch, getState) => {
    const url = `/api/${options.model}/${id}/${relation.name}/${fk}`;
    return axios.patch(url, data, getHeaders(getState))
      .then((res) => {
        dispatch(action.update(res.data));
        return res.data;
      });
  };

  services[`${relation.name}$delete`] = (id, fk) => (dispatch, getState) => {
    const url = `/api/${options.model}/${id}/${relation.name}/${fk}`;
    return axios.delete(url, getHeaders(getState))
      .then((res) => {
        dispatch(action.remove({ id: fk }));
        return res.data;
      });
  };

  return services;
};

export default (options) => {
  const action = actions[options.name];

  const services = {};

  services.create = data => (dispatch, getState) => {
    const url = `/api/${options.model}`;
    return axios.post(url, data, getHeaders(getState))
      .then((res) => {
        dispatch(action.create(res.data));
        return res.data;
      });
  };

  services.update = (id, data) => (dispatch, getState) => {
    const url = `/api/${options.model}/${id}`;
    return axios.patch(url, data, getHeaders(getState))
      .then((res) => {
        dispatch(action.update(res.data));
        return res.data;
      });
  };

  services.find = (filter = {}) => (dispatch, getState) => {
    const url = `/api/${options.model}?filter=${encodeURIComponent(JSON.stringify(filter))}`;
    return axios.get(url, getHeaders(getState))
      .then((res) => {
        dispatch(action.createList(res.data));
        return res.data;
      });
  };

  services.findOne = (id, filter = {}) => (dispatch, getState) => {
    const url = `/api/${options.model}/${id}?filter=${encodeURIComponent(JSON.stringify(filter))}`;
    return axios.get(url, getHeaders(getState))
      .then((res) => {
        dispatch(action.create(res.data));
        return res.data;
      });
  };

  services.findById = id => (dispatch, getState) => {
    const url = `/api/${options.model}/${id}`;
    return axios.get(url, getHeaders(getState))
      .then((res) => {
        dispatch(action.create(res.data));
        return res.data;
      });
  };

  services.delete = id => (dispatch, getState) => {
    const url = `/api/${options.model}/${id}`;
    return axios.delete(url, getHeaders(getState))
      .then((res) => {
        dispatch(action.remove({ id }));
        return res.data;
      });
  };

  lodash.forEach(options.relations, (relation) => {
    Object.assign(services, generateRelation(options, relation));
  });

  return services;
};
