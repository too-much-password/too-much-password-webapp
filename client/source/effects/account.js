import axios from 'axios';
import generate from './crud';
import account from '../actions/account';
import authentication from '../actions/authentication';

const services = generate({
  name: 'account',
  model: 'accounts',
  relations: [
    { name: 'cipherKeys', type: 'hasMany', action: 'cipherKey' },
    { name: 'ownedPasswords', type: 'hasMany', action: 'password' },
    { name: 'passwords', type: 'hasMany', action: 'password' },
  ],
});

services.login = data => dispatch => axios
  .post('/api/accounts/login', data)
  .then((res) => {
    dispatch(authentication.create(res.data));
    return res.data;
  });

services.logout = () => (dispatch, getState) => {
  const token = getState().authentication;
  if (!token) return Promise.reject('Not authenticated');
  return axios
    .post('/api/accounts/logout', {}, {
      headers: {
        Authorization: token.id,
      },
    })
    .then(() => dispatch(authentication.remove()));
};

services.search = query => (dispatch, getState) => {
  const token = getState().authentication;
  if (!token) return Promise.reject('Not authenticated');
  return axios
    .get(`/api/accounts/search/${encodeURIComponent(query)}`, {
      headers: {
        Authorization: token.id,
      },
    })
    .then((res) => {
      dispatch(account.create(res.data));
      return res.data;
    });
};

export default services;
