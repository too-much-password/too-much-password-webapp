import generate from './crud';

export default generate({
  name: 'password',
  model: 'passwords',
  relations: [
    { name: 'cipherKeys', type: 'hasMany', action: 'cipherKey' },
  ],
});
