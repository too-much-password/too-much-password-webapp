import constants from '../constants.json';

export const defaultOptions = {
  create: true,
  createList: true,
  update: true,
  updateList: true,
  remove: true,
};

export default (options = {}) => {
  const opts = { ...defaultOptions, ...options };
  const cst = constants[options.name];
  const actions = {};
  if (opts.create) {
    actions.create = payload => ({ type: cst.CREATE, payload });
  }
  if (opts.createList) {
    actions.createList = payload => ({ type: cst.CREATE_LIST, payload });
  }
  if (opts.update) {
    actions.update = payload => ({ type: cst.UPDATE, payload });
  }
  if (opts.updateList) {
    actions.updateList = payload => ({ type: cst.UPDATE_LIST, payload });
  }
  if (opts.remove) {
    actions.remove = payload => ({ type: cst.REMOVE, payload });
  }
  return actions;
};
