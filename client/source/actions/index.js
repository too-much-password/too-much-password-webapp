import account from './account';
import cipherKey from './cipher-key';
import password from './password';

export default {
  account,
  cipherKey,
  password,
};
