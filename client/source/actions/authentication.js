import generate from './crud';

export default generate({
  name: 'authentication',
  createList: false,
  update: false,
  updateList: false,
});
