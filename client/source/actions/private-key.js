import { privateKey as cst } from '../constants.json';

const update = payload => ({ type: cst.UPDATE, payload });

export default { update };
