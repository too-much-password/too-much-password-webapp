import { notification as cst } from '../constants.json';

export const set = event => ({ type: cst.SET, event });
export const unset = () => ({ type: cst.UNSET });
