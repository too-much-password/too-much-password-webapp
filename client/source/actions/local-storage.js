export function load(key, defaultValue) {
  if (typeof localStorage !== 'undefined') {
    return JSON.parse(localStorage.getItem(key));
  }
  console.warn('Localstorage not supported !');
  return defaultValue;
}

export function persist(key, value) {
  if (typeof localStorage !== 'undefined') {
    localStorage.setItem(key, JSON.stringify(value));
  } else {
    console.warn('Localstorage not supported !');
  }
}
