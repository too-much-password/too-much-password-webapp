import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { IndexRoute, Router, Route, hashHistory } from 'react-router';
import injectTapEventPlugin from 'react-tap-event-plugin';

import { deepOrange500 } from 'material-ui/styles/colors';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import AuthenticationView from './containers/authentication-view';
import HomeView from './containers/home-view';
import KeypassView from './containers/keypass-view';
import ChangePasswordView from './containers/change-password-view';
import PasswordCreateView from './containers/password-create-view';
import PasswordRootView from './containers/password-root-view';
import PasswordShareView from './containers/password-share-view';
import PasswordShowView from './containers/password-show-view';
import Root from './containers/root';

import './locale';
import configureStore from './store';

const store = configureStore(hashHistory);
const history = syncHistoryWithStore(hashHistory, store);

injectTapEventPlugin();

const muiTheme = getMuiTheme({
  palette: {
    accent1Color: deepOrange500,
  },
});

render(
  <Provider store={store}>
    <MuiThemeProvider muiTheme={muiTheme}>
      <Router history={history}>
        <Route component={Root} path="/">
          <IndexRoute component={HomeView} />
          <Route component={KeypassView} path="keypass" />
          <Route component={ChangePasswordView} path="change-password" />
          <Route component={PasswordCreateView} path="password/create" />
          <Route component={PasswordRootView} path="password/:id">
            <Route component={PasswordShareView} path="share" />
            <Route component={PasswordShowView} path="show" />
          </Route>
        </Route>
        <Route component={AuthenticationView} path="/authentication" />
      </Router>
    </MuiThemeProvider>
  </Provider>,
  document.getElementById('content'),
);

if (window.loading_screen) {
  window.loading_screen.finish();
}
