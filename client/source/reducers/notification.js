import { notification as types } from '../constants.json';

export const initialState = {
  isOpen: false,
  timeout: 3000,
  message: '',
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case types.SET:
      return {
        ...initialState,
        ...action.event,
        isOpen: true,
      };
    case types.UNSET:
      return { ...initialState };
    default:
      return state;
  }
}
