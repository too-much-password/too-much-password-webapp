import { authentication as cst } from '../constants.json';

export default (state = null, action) => {
  switch (action.type) {
    case cst.CREATE:
      return action.payload;
    case cst.REMOVE:
      return null;
    default:
      return state;
  }
};
