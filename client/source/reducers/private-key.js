import { privateKey as cst } from '../constants.json';
import { load, persist } from '../actions/local-storage';

export const initialState = {
  persist: false,
  content: null,
  ...load('private-key'),
};

export default function reducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    case cst.UPDATE:
      newState = {
        ...state,
        ...action.payload,
      };
      persist('private-key', action.payload ? newState : null);
      return newState;
    default:
      return state;
  }
}
