import lodash from 'lodash';
import constant from '../constants.json';

export default config => (state = {}, action) => {
  switch (action.type) {
    case constant[config.name].CREATE:
      return { ...state, [action.payload.id]: action.payload };
    case constant[config.name].UPDATE:
      return {
        ...state,
        [action.payload.id]: {
          ...state[action.payload.id],
          ...action.payload,
        },
      };
    case constant[config.name].CREATE_LIST:
    case constant[config.name].UPDATE_LIST:
      return lodash.reduce(action.payload, (db, value) => ({ ...db, [value.id]: value }), state);
    case constant[config.name].REMOVE:
      return lodash.omit(state, `${action.payload.id}`);
    default:
      return state;
  }
};
