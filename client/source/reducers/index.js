import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import account from './account';
import authentication from './authentication';
import cipherKey from './cipher-key';
import language from './language';
import notification from './notification';
import password from './password';
import privateKey from './private-key';

export default combineReducers({
  account,
  authentication,
  cipherKey,
  language,
  notification,
  password,
  privateKey,
  routing: routerReducer,
});
