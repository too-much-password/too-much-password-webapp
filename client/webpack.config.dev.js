const path = require('path');
const webpack = require('webpack');

const clientPath = __dirname;
const sourcePath = path.join(__dirname, 'source');
const buildPath = path.join(clientPath, 'build');

module.exports = {
  entry: [
    'webpack/hot/dev-server',
    path.join(sourcePath, 'main.jsx')
  ],
  resolve: {
    extensions: ['', '.js', '.jsx', '.json']
  },
  output: {
    path: buildPath,
    filename: './bundle.js'
  },
  devtool: 'source-map',
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:3000',
        secure: false
      }
    },
    contentBase: buildPath,
    hot: true
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel',
        include: [ sourcePath ],
        query: {
          cacheDirectory: true
        }
      },
      {
        test: /\.json$/,
        loader: 'json'
      }
    ],
    preLoaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'eslint',
        include: [ sourcePath ]
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('development'),
        'ROLLBAR_ENABLED': JSON.stringify(true),
        'ROLLBAR_ACCESSTOKEN': JSON.stringify('69f7b78f46c34b94a06b994914e0059f'),
      }
    }),
  ]
};
