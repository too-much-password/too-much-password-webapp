const path = require('path');
const webpack = require('webpack');

const clientPath = __dirname;
const sourcePath = path.join(clientPath, 'source');
const buildPath = path.join(clientPath, 'build');

module.exports = {
  entry: [
    path.join(sourcePath, 'main.jsx')
  ],
  resolve: {
    extensions: ['', '.js', '.jsx', '.json']
  },
  output: {
    path: buildPath,
    filename: './bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel',
        include: [ sourcePath ],
        query: {
          cacheDirectory: true
        }
      },
      {
        test: /\.json$/,
        loader: 'json'
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production'),
        'ROLLBAR_ENABLED': JSON.stringify(true),
        'ROLLBAR_ACCESSTOKEN': JSON.stringify(process.env.ROLLBAR_CLIENT_ACCESSTOKEN),
      }
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compress: {
        warnings: true
      }
    })
  ]
};
