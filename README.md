# Too Much Password Web Application

[![Gitlab CI](https://circleci.com/gh/too-much-password/too-much-password-webapp.svg?style=shield)](https://circleci.com/gh/too-much-password/too-much-password-webapp)
[![Codecov](https://img.shields.io/codecov/c/github/too-much-password/too-much-password-webapp.svg?maxAge=2592000)](https://codecov.io/gh/too-much-password/too-much-password-webapp)

## Roadmap

- Give permissions to users

## Tools

### Continuous integration

[![Browserstack](/documentation/assets/browserstack-logo.png)](https://www.browserstack.com)
[![Nightwatch](http://nightwatchjs.org/img/logo-nightwatch.png)](http://nightwatchjs.org/)

### Deployment and servers

[![Rollbar](https://worldvectorlogo.com/logos/rollbar.svg)](https://rollbar.com/)
[![Heroku](https://upload.wikimedia.org/wikipedia/en/a/a9/Heroku_logo.png)](https://heroku.com)
